###################################################################################################
                    Welcome to eChaupaal
 eChaupaal is a Whatsapp bot to track market price of commodities & weather updates.This application can help farmers getting the track of market price of all the vegetables,creals etc.A
Also it can help them in protecting their crops from getting destroyed due to suddenrains etc by getting the weather updates for the next 24 hrs.
Bot contain some simple commands to operate for farmers and we are planning to come with hindi version of this as well.

This application will help farmers and consumers to know market price of commodities in various markets and also allows farmers to get recent updates on weather of their city. 
This will help them to take protective measures against heavy rains, storms etc.

To use eChaupaal all you have to do is:
1.Save eChaupaal number to your mobile contacts.
2.You will now see eChaupaal in your whatsapp contact list.
3.write 'help' and send to it.
4.You will recieve list of available commands.
5.Based on the requirement you can use the appropriate command.

 eChaupaal number = +917617827278
 Also you can visit "http://e-chaupaal.appspot.com" to see the landing page.

Thank you.
eChaupaal

#######################################################################################################