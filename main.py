import webapp2
import logging
import json
import re
import httplib, urllib
from google.appengine.ext import vendor
from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.api import mail
from google.appengine.api import urlfetch
from google.appengine.ext import vendor
from datetime import date
from datetime import datetime
import unicodedata
import shlex
vendor.add('libs')
import sys
sys.path.insert(0, 'libs')
from bs4 import BeautifulSoup


class commodities(db.Model):
        name = db.StringProperty()
        type = db.StringProperty()
        lastdumpeddate= db.DateProperty(auto_now_add = True)
        
class users(db.Model):
        user = db.StringProperty()
        market = db.StringProperty()
        joined_on = db.DateProperty(auto_now_add = True)
        last_request = db.StringProperty()
        request = db.StringProperty()
        

class commoditydata(db.Model):
        commodity= db.StringProperty()
        market= db.StringProperty()
        arrival= db.StringProperty()
        variety= db.StringProperty()
        minprice= db.StringProperty()
        maxprice= db.StringProperty()
        modalprice= db.StringProperty()
        reporteddate= db.DateTimeProperty(auto_now_add = True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
        commodity = commodities(name = 'cucumber', type = 'vegetable')
        commodity.put()



class FetchHandler(webapp2.RequestHandler):
    def post(self):
        today = date.today()
        items = commodities.all()
        items.filter('lastdumpeddate !=',today)
        for item in items.fetch(limit = 1):
            logging.info(item.name)
            params= urllib.urlencode({'cmm': item.name, 'mkt': '', 'search': ''})
            headers= {'Cookie': 'ASPSESSIONIDCCRBQBBS=KKLPJPKCHLACHBKKJONGLPHE; ASP.NET_SessionId=kvxhkhqmjnauyz55ult4hx55; ASPSESSIONIDAASBRBAS=IEJPJLHDEKFKAMOENFOAPNIM','Origin': 'http://agmarknet.nic.in', 'Accept-Encoding': 'gzip, deflate', 'Accept-Language': 'en-GB,en-US;q=0.8,en;q=0.6','Upgrade-Insecure-Requests': '1','User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36', 'Content-Type': 'application/x-www-form-urlencoded','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'Cache-Control': 'max-age=0','Referer': 'http://agmarknet.nic.in/mark2_new.asp','Connection': 'keep-alive'}
            conn= httplib.HTTPConnection("agmarknet.nic.in")
            conn.request("POST", "/SearchCmmMkt.asp", params, headers)
            urlfetch.set_default_fetch_deadline(60)
            response= conn.getresponse()
            data= response.read()
            soup = BeautifulSoup(data,"html.parser")
            c = ''
            x = ''
            y = []
            index = 0
            trs = soup.findAll("tr")
            for tr in trs:
                c = unicodedata.normalize('NFKD', tr.text)
                y.append(str(c))
            for x in y:
                if index>3:
                    data1 = x.split()
                    if len(data1) == 6 :
                        commodity = commoditydata(commodity = item.name.lower() , market = data1[0].lower(),arrival = data1[1],variety = data1[2],minprice = data1[3], maxprice = data1[4],modalprice = data1[5])
                        commodity.put()
                index = index + 1
            k = commodities.all()
            k.filter('name =',item.name)
            q = k.get()
            q.lastdumpeddate = date.today()
            q.put()
            #logging.info(q.lastdumpeddate)
            
            
            
        
class FetchTriggerStart(webapp2.RequestHandler):
	def get(self):
		taskqueue.add(url = '/fetch/',params = {})
		self.response.write("Fetch trigger initiated")
           
    
app = webapp2.WSGIApplication([
    ('/get_rates/', MainHandler),('/fetch/', FetchHandler),('/fetch/start/',FetchTriggerStart)
], debug=True)
