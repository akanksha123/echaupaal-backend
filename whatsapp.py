import webapp2
import logging
import json
import re
import httplib, urllib
from google.appengine.ext import vendor
from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.ext import ndb
from google.appengine.api import mail
from google.appengine.api import urlfetch
from google.appengine.ext import vendor
from datetime import date
import datetime
import unicodedata
import shlex
vendor.add('libs')
import sys
sys.path.insert(0, 'libs')
from bs4 import BeautifulSoup


class commodities(ndb.Model):
        name = ndb.StringProperty()
        type = ndb.StringProperty()
        lastdumpeddate= ndb.DateProperty(auto_now_add = True)
        
class users(ndb.Model):
        user = ndb.StringProperty()
        market = ndb.StringProperty()
        joined_on = ndb.DateProperty(auto_now_add = True)
        last_request = ndb.StringProperty()
        request = ndb.StringProperty()
        

class commoditydata(ndb.Model):
        commodity= ndb.StringProperty()
        market= ndb.StringProperty()
        arrival= ndb.StringProperty()
        variety= ndb.StringProperty()
        minprice= ndb.StringProperty()
        maxprice= ndb.StringProperty()
        modalprice= ndb.StringProperty()
        reporteddate= ndb.DateProperty(auto_now_add = True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
        command = self.request.get('c',"Command not found")
        comm_name = command.split()
        command = command.lower()
        command_split = command.split()
        logging.info(command_split[0])
        if command_split[0] == 'weather':
            if len(command_split) == 2:
                self.get_weather(command_split[1])
            else:
                self.response.write("Command not found.")
                self.response.write("\xF0\x9F\x98\x92")
                self.response.write("\n")
                self.response.write("help - ")
                self.response.write("weather cityname")
        elif command_split[0] == 'setmarket':
            logging.info(command)
            user = self.request.get('user',"No user found")
            command = command.split()
            if len(command) == 2:
                set_market = comm_name[1]
                market_list = commoditydata.query(commoditydata.market == set_market)
                market_exist = market_list.get()
                if market_exist is not None:
                    userlist = users.query(users.user == user)
                    q = userlist.get()
                    for i in userlist.fetch(limit=1):
                        i.market = set_market
                        i.put()
                    self.response.write("Market set successfully")
                    self.response.write("\xF0\x9F\x91\x8D")
                else:
                    self.response.write("Market name not found.Use")
                    self.response.write("\n")
                    self.response.write("► list market")
                    self.response.write("\n")
                    self.response.write("command to get list of all available markets")
                    
            else:
                self.response.write("Command not found.")
                self.response.write("\xF0\x9F\x98\x92")
                self.response.write("\n")
                self.response.write("help - ")
                self.response.write("setmarket  marketname")
                
        elif command_split[0] == 'list':
            if len(command_split) == 2 :
                self.get_list(command_split[1])
            else:
                self.response.write("Invalid command")
                self.response.write("\xF0\x9F\x98\x92")
                self.response.write("\n")
                self.response.write("help:")
                self.response.write("\n")
                self.response.write("► ")
                self.response.write("list items")
                self.response.write("\n")
                self.response.write("► ")
                self.response.write("list market")
     
        else:
            if command_split[0] == 'help':
                        self.response.write("Available commands are:")
                        self.response.write("\n")
                        self.response.write("\x31\xE2\x83\xA3")
                        self.response.write("  setmarket <marketname>")
                        self.response.write("\n")
                        self.response.write("       Example : setmarket bilaspur")
                        self.response.write("\n")
                        self.response.write("\n")
                        self.response.write("\x32\xE2\x83\xA3")
                        self.response.write("<commodityname> <marketname>")
                        self.response.write("\n")
                        self.response.write("       Example : onion nagpur")
                        self.response.write("\n")
                        self.response.write("\n")
                        self.response.write("\x33\xE2\x83\xA3")
                        self.response.write("  weather <cityname>")
                        self.response.write("\n")
                        self.response.write("       Example : weather bangalore")
                        self.response.write("\n")
                        self.response.write("\n")
                        self.response.write("\x34\xE2\x83\xA3")
                        self.response.write("  list items")
                        self.response.write("\n")
                        self.response.write("\x35\xE2\x83\xA3")
                        self.response.write("  list markets")
            else:    
                user = self.request.get('user',"No user found")
                if len(command_split) == 2:
                    self.get_commoditydata(command_split[0],user,comm_name[1])
                else:
                    q = commodities.query(commodities.name == command_split[0])
                    k = q.get()
                    userdata = users.query(users.user == user)
                    for i in userdata.fetch(limit = 1):
                        market = i.market
                    if k is not None:
                        self.get_commoditydata(command_split[0],user,market)
                    else:
                        self.response.write("Available commands are:")
                        self.response.write("\n")
                        self.response.write("\x31\xE2\x83\xA3")
                        self.response.write("  setmarket <marketname>")
                        self.response.write("\n")
                        self.response.write("       Example : setmarket bilaspur")
                        self.response.write("\n")
                        self.response.write("\n")
                        self.response.write("\x32\xE2\x83\xA3")
                        self.response.write("<commodityname> <marketname>")
                        self.response.write("\n")
                        self.response.write("       Example : onion nagpur")
                        self.response.write("\n")
                        self.response.write("\n")
                        self.response.write("\x33\xE2\x83\xA3")
                        self.response.write("  weather <cityname>")
                        self.response.write("\n")
                        self.response.write("       Example : weather bangalore")
                        self.response.write("\n")
                        self.response.write("\n")
                        self.response.write("\x34\xE2\x83\xA3")
                        self.response.write("  list items")
                        self.response.write("\n")
                        self.response.write("\x35\xE2\x83\xA3")
                        self.response.write("  list markets")
           
    
    def get_commoditydata(self,name,user,market):
            q = commodities.query(commodities.name == name)
            k = q.get()
            logging.info(market)
            if k is not None:
                logging.info("inside k block")
                qry = commoditydata.query(commoditydata.commodity == name,(commoditydata.market) == market)
                logging.info(qry)
                q = qry.get()
                if q is not None:
                    price_list = []
                    for result in qry.fetch(limit = 1):
                        logging.info(result.market)
                        self.response.write('Item searched =  ')
                        self.response.write(name)
                        self.response.write("\n")
                        self.response.write('Market =  ')
                        self.response.write(result.market)
                        self.response.write("\n")
                        self.response.write('Arrival = ')
                        self.response.write(result.arrival)
                        self.response.write("  (Tonnes)")
                        self.response.write("\n")
                        price_list.append(result.maxprice)
                        price_list.append(result.minprice)
                        price_list = [int(i) for i in price_list]
                        self.response.write('Max price (per kg) = Rs.')
                        self.response.write((price_list[0]/100))
                        self.response.write("\n")
                        self.response.write('Min price (per kg) = Rs.')
                        self.response.write((price_list[1]/100))
                        self.response.write("\n")
                        self.response.write('Reported date = ')
                        self.response.write(result.reporteddate)
                else:
                        self.response.write("\xF0\x9F\x98\xB3")
                        self.response.write("No data received for this market name. Check with")
                        self.response.write("\n")
                        self.response.write("► list market")
                        self.response.write("\n")
                        self.response.write("to see if market name is entered correctly")
            else:
                    self.response.write("Invalid command")
                    self.response.write("\xF0\x9F\x98\x92")
                    self.response.write("\n")
                    self.response.write("help -- ")
                    logging.info(k)
                    self.response.write("commodityname marketname")
                    userdetail = users.query(users.user ==user)
                    userdeatil1 = userdetail.get()
                    if userdeatil1 is not None:
                        for item in userdetail.fetch(limit = 1):
                            userdeatil1.last_request = userdeatil1.request
                            userdeatil1.market = market
                            userdeatil1.request = name
                            userdeatil1.put()
                    else:
                        usernumber = users(user = user,market = market,request = name,last_request = name)
                        usernumber.put()
                        logging.info(user)
                        
    # To get weather details
    def get_weather(self,city_name):
        params = urllib.urlencode({'q' : city_name,'mode':'json','units':'metric','APPID': 'd4e19306eb1dcc7690ae2279ea7540c2'})
        conn = httplib.HTTPConnection("api.openweathermap.org")
        conn.request("POST", "/data/2.5/forecast?"+params)
        response = conn.getresponse()
        data = response.read()
        response = json.loads(data)
        i = 0
        self.response.write("\xE2\x98\x80")
        self.response.write("\xE2\x98\x81")
        self.response.write("Weather forcast for next 24 hours")
        self.response.write("\n")
        self.response.write("City = "+city_name)
        self.response.write("\n")
        self.response.write("Description :")
        self.response.write("\n")
        for item in response:
            if item == 'list':
                for i in range(1,9):
                    desc = response['list'][i]['weather'][0]['description']
                    if desc == "sky is clear":
                        self.response.write("\xE2\x98\x80")
                    else:
                        self.response.write("\xE2\x98\x81")
                    date_string = str(response['list'][i]['dt_txt'])
                    temp = response['list'][i]['main']['temp_max']
                    self.response.write(desc)
                    self.response.write(" (")
                    self.response.write(temp)
                    self.response.write("°C)")
                    self.response.write("\n")
                    time = datetime.datetime.strptime(date_string, "%Y-%m-%d %H:%M:%S")
                    day = time.strftime("%A %H:%M")
                    self.response.write("on  "+day)
                    logging.info(day)
                    self.response.write("\n")
                i = i + 1
                
                
    #get list of market and commodity
    
    def get_list(self,item_searched):
        market_list = []
        final_marketlist = []
        if item_searched == 'items':
            record = commodities.query()
            self.response.write("List of available items:")
            for item in record.fetch(limit = 9) :
                self.response.write("\n")
                self.response.write("\xF0\x9F\x8D\x85")
                self.response.write(item.name)
        elif item_searched == 'markets':
            record = commoditydata.query()
            for item in record.fetch(limit = record.count()):
                market_list.append(item.market)
            for name in market_list:
                if name not in final_marketlist:
                    final_marketlist.append(name)
            self.response.write("List of available markets:")
            for i in final_marketlist:
                self.response.write("\n")
                self.response.write("\xF0\x9F\x8C\xBE")
                self.response.write(i)
        else:
                self.response.write("Invalid command")
                self.response.write("\xF0\x9F\x98\x92")
                self.response.write("\n")
                self.response.write("help:")
                self.response.write("\n")
                self.response.write("► ")
                self.response.write("list items")
                self.response.write("\n")
                self.response.write("► ")
                self.response.write("list market")
            
            
                
                
            
            
        
    
    
        
        
        
app = webapp2.WSGIApplication([
    ('/whatsapp/', MainHandler)], debug=True)
